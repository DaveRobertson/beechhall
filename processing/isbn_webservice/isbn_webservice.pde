/*

This is the websrvice PDE Sketch to be included

<ISBNdb server_time="2012-09-16T18:26:45Z">
<BookList total_results="1" page_size="10" page_number="1" shown_results="1">
<BookData book_id="programming_web_services_with_perl" isbn="0596002068" isbn13="9780596002060">
<Title>Programming Web services with Perl</Title>
<TitleLong/>
<AuthorsText>Randy J. Ray and Pavel Kulchenko</AuthorsText>
<PublisherText publisher_id="oreilly">Farnham ; O'Reilly, 2002 printing, c2003.</PublisherText>
</BookData>
</BookList>
</ISBNdb>

*/


XML xml;


// Used for testing
void setup()
{
  BHBook myBook = new BHBook();
  println("Calling book data");
  GetBookData("9780099189619",myBook) ;
  
  println(myBook);
  
}


void GetBookData(String ISBNID, BHBook theBook) {
  
  //xml =  loadXML( "http://isbndb.com/api/books.xml?access_key=BEAOK8NJ&index1=isbn&value1=" +ISBNID);
  xml =  loadXML( "books.xml");
  
  int numSites = xml.getChildCount();
  println(numSites);
  //println(xml.getName ());
  for (int i = 0; i < numSites; i++) {
   
   XML node = xml.getChild(i);
   //println("Node:"+node);
   // Look for the BooList node names - could be multiple books ? 
   if ( node.getName().equals("BookList") )
   {
     //println("BookList found");
     int numbooks = node.getChildCount();
     //println("Numbooks:"+numbooks);
     
     for (int j = 0; j < numbooks; j++) {

         XML booknode = node.getChild(j);
         
         //println(booknode.getName());
         //println(booknode);
         if (booknode.getName().equals("BookData") ) {
         
           String book_id = booknode.getString("book_id");
           String isbn_number = booknode.getString("isbn");
           String isbn_number13= booknode.getString("isbn13");
           theBook.ID=book_id;
           println(book_id);
           println(isbn_number);
           println(isbn_number13);
           
           //println("BookList found");
           
           int numdata = booknode.getChildCount();
           
           for (int k = 0; k < numdata; k++) {
               
             XML bookdatanode = booknode.getChild(k);
             
             if (bookdatanode.getName().equals("Title") ) {
               String title= bookdatanode.getContent();
               println("Title: " + title);
           
             }
             if (bookdatanode.getName().equals("AuthorsText") ) {
               String AuthorsText= bookdatanode.getContent();
               println("AuthorsText: " + AuthorsText);
           
             }
             if (bookdatanode.getName().equals("PublisherText ") ) {
               String PublisherText = bookdatanode.getContent();
               println("PublisherText : " + PublisherText );
           
             }
             
           }           
     
         }
             
     } 
   } 
       
  }
}


/*

From the ISBN Database

<BookData book_id="programming_web_services_with_perl" isbn="0596002068" isbn13="9780596002060">
<Title>Programming Web services with Perl</Title>
<TitleLong/>
<AuthorsText>Randy J. Ray and Pavel Kulchenko</AuthorsText>
<PublisherText publisher_id="oreilly">Farnham ; O'Reilly, 2002 printing, c2003.</PublisherText>

*/

class BHBook { 
  String ID;
  String ISBN;
  String ISBN13;
  String Author;
  String Publisher;
  
  BHBook() {
     ID="Not Found";
    String ISBN="";
    String ISBN13="";
    String Author="";
    String Publisher="";
  }
  
  String toString()  {
    println("Beech Hall Book ");
     println("ID="  + ID);
    return  "Test";
  }
   
} 


