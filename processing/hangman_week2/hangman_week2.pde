//
//  Week 1 - Example Exercise HangMan Basic
//

PImage man; 

void setup()
{
size(400,400);
background(255,255,255);

line(100,50,100,100);
ellipse(100,50,30,30);
line(100,100,115,115);
line(85,115,100,100);
line(75,75,100,75);

line(100,30,100,20);
line(100,20,50,20);
line(50,20,50,140);
line(0,140,200,140);

 // Draw the lines for the words
line(100,300,120,300);
line(140,300,160,300);
line(180,300,200,300);
line(100,350,120,350);
line(140,350,160,350);

// Extra text and image – image file in sketch folder
man = loadImage("image.jpg");

//font = loadFont("LetterGothicStd-32.vlw");
//textFont(font);
}
void draw()
{
   
  fill(50);
  image(man,300,50,100,100);
  
// Text wraps within text box
  text("Enter a letter for a guess", 100, 200);  

}

