

/*
 
 EAN/UPC Reader (C) Dave Robertson 2012
 
http://eandata.com/feed.php?keycode=B406E5014FA42631&mode=json&find=049000000443
  
status
    find	    the find value you sent in your request
    code	    200 = success, 404 = not found, 400 and 500 = error
    message	    details about the code
    version	    feed version number
product
    modified	    YYYY-MM-DD HH:MM:SS (24 hour time)
    ean13	    13 digit EAN-13 code
    upca	    12 digit UPC-A code (may be n/a)
    upce	    8 digit UPC-E code (may be n/a)
    product	    the name of the product
    description	    size, weight, count, color, etc.
    category_no	    numeric category code
    category_text	    text version of the category
    url	    a link to the product page (not on our site)
    image	    a link to an image of the product (on our site)
    has_long_desc	    does this product have a long description (1/0)
    barcode	    a link to a barcode image (on our site)
    locked	    is this data locked from updates (1/0)
company
    name	    company name
    logo	    a link to a company logo (on our site)
    url	    a link to the company's official web site
    address	    the company's mailing address
    phone	    the company's phone number (digits only)
    locked	    is this data locked from updates (1/0)
 
Long Description
 
http://eandata.com/feed.php?keycode=B406E5014FA42631&mode=json&find=000000151122&get=long_desc

 
status
    find	    the find value you sent in your request
    code	    200 = success, 404 = not found, 400 and 500 = error
    message	    details about the code
    version	    feed version number
product
    modified	    YYYY-MM-DD HH:MM:SS (24 hour time)
    long_desc	    the long description for the product
    locked	    is this data locked from updates (1/0)
 
 */

import guicomponents.*;
import org.json.*;

PImage onlineImage;
PImage onlineBarcodeImage;
String imageURL;
String barcodeImageURL;


void setup() {

  size(700,400);
  createGUI();
  imageURL="";
  barcodeImageURL="";
  
  
  
}

void getBarcode(String barCode)
//
//  Get the barcode from the external database.
//  We will update the presentaion layer from here/
//
{
  // Accessing the weather service
  String BASE_URL = "http://weather.yahooapis.com/forecastjson?w=";
  String WOEID = "898091";

  // Get the JSON formatted response
 //String response = loadStrings( BASE_URL + WOEID )[0];

  //println(response);
  
  String eanURL="http://eandata.com/feed.php?keycode=B406E5014FA42631&mode=json&find=0054800261048";
  String eanlongURL= "http://eandata.com/feed.php?keycode=B406E5014FA42631&mode=json&find=000000151122&get=long_desc";
  
  //String response = loadStrings( eanURL)[0];

  //String response = loadStrings( "ean long.txt")[0];
  String response = loadStrings( "ean.txt")[1];

  
 //println(response);

  //String url = "http://processing.org/img/processing_beta_cover.gif";
  //online = loadImage(url, "png");


  // Make sure we got a response.
  if ( response != null ) {
    // Initialize the JSONObject for the response
    JSONObject root = new JSONObject( response );


    // Get the "status" JSONObject
    JSONObject status = root.getJSONObject("status");
   // println(status);35
   
   

    // Get the "product" JSONObject
    JSONObject product = root.getJSONObject("product");
    //println(product);


    // Get the "product" JSONObject
    JSONObject company = root.getJSONObject("company");
    //println(company);


    //println(product.get("barcode"));
    
 
  

    imageURL = product.get("image").toString();
    barcodeImageURL = product.get("barcode").toString();
    
    println(imageURL);
    println(barcodeImageURL);
    
    
    if (imageURL.length() > 0) {
      onlineImage = loadImage(imageURL, "png");
      image(onlineImage, 0, 0);
      
    }
    if (barcodeImageURL.length() > 0) {
    
      onlineBarcodeImage = loadImage(barcodeImageURL, "png");
      println(onlineBarcodeImage);
      image(onlineBarcodeImage, 400, 100);
    
  }

    
    
    //println( product );
    
    //println(product.get("image"));   
    //println(product.get("product"));
    //println(product.get("description"));
    
    
    
  }
}

void draw() {

    
    if (imageURL.length() > 0) {
      
      image(onlineImage, 0, 0);
      
    }
    
    if (barcodeImageURL.length() > 0) {
    
      image(onlineBarcodeImage, 400, 100);
    
  }


}




void BarCodeStart()

{
  
  
}


