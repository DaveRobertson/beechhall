// The following short XML file called "sites.xml" is parsed 
// in the code below. It must be in the project's "data" directory
// <?xml version="1.0"?>
// <websites>
//   <site id="0" url="processing.org">Processing</site>
//   <site id="1" url="mobile.processing.org">Processing Mobile</site>
// </websites>

XML xml;

void setup() {
  size(200, 200);
  xml =  loadXML( "sites.xml");
  int numSites = xml.getChildCount();
  println(numSites);
  for (int i = 0; i < numSites; i++) {
    XML kid = xml.getChild(i);
    //println(kid.getChildCount());   
    //println(kid.getChildren().length); 
    println(kid);
    if ( kid.getChildCount() > 0 )
   { 
      int id = kid.getInt("id"); 
      String url = kid.getString("url"); 
      String site = kid.getContent();
      println(id + " : " + url + " : " + site);
   }    
  }
}
