/**
 *
 *  Barcode reader
 * 
 */
 
int max_height = 20;
int min_height = 10;
int letter_height = max_height; // Height of the letters
int letter_width = 10;          // Width of the letter

int x = -letter_width;          // X position of the letters
int y = 0;                      // Y position of the letters

boolean newletter;              

int numChars = 26;      // There are 26 characters in the alphabet
color[] colors = new color[numChars];
boolean barCodeStarted=false;
String barCode="";    // String of barcode
int savedTime; // When Barcode chars first detected
int totalTime; // How long Timer should last
int currentTime; // How long Timer should last

void setup()
{
  size(200, 200);
  noStroke();
  colorMode(RGB, numChars);
  background(numChars/2);
  // Set a gray value for each key
  for(int i=0; i<numChars; i++) {
    colors[i] = color(i, i, i);    
  }
}

void draw()
{
  if(newletter == true) {
   
     println (barCode); 
    }

    newletter = false;

    if (barCodeStarted) {
      currentTime = millis(); 
      totalTime = currentTime-savedTime;
      //println( currentTime);
      if ( totalTime >1000 )
     {
        barCodeStarted=false;
        println ("Final barcode: "+barCode);
     }   
    }
  
  
}

void keyPressed()
{
  // if the key is between 'A'(65) and 'z'(122)
  if (!barCodeStarted){
   
    savedTime = millis(); 
    barCodeStarted=true;
  }
  
//  if( key >= 'A' && key <= 'z') {
    // Valid key from reader
    barCode = barCode + key;

  //}

  newletter = true;
  
    
}
