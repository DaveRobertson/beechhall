/*
 * XmlrpcServer example
 * It includes only the basic functionalities for xml-rpc protocol.
 * @author: Burak Arikan
 */

import xmlrpclib.*;

XmlrpcServer server;
int port = 8081;

int num = 200;
float m[];
float inc = 0.0;
int speed = 1;

void setup(){
  size(400,400);
  noStroke();
  smooth();
  ellipseMode(CENTER);

  m = new float[num];
  for(int i=0; i<num; i++) {
    m[i] = 0.1f;
  }

  // Initialize the server
  server = new XmlrpcServer(port);

  // Add a handler and an Object. 
  // By adding "this" you open all the public methods in this applet
  // You can also add any other Object
  // All the public methods of the Object will be available through XML-RPC
  server.add("testserver", this);

  // Note that /RPC2 is hard-coded to URI in the Apache Library.
  // For example, you can invoke this server at http://18.83.20.106:8081/RPC2
  println("Server started at " + server.ip()+":"+port+"/RPC2");
}


void draw(){
  background(72);

  inc += 0.001*speed;

  for(int i=1; i<num; i++) {
    m[i-1] = m[i];
  } 
  m[num-1] = inc;

  beginShape(LINE_STRIP);
  for(int i=0; i<num; i++){
    stroke(255);
    float x = 200+120*cos(m[i]);
    float y = 200+120*sin(m[i]);
    vertex(x, y);
  }
  endShape();

}

// This method can be remotely called from another program
public String update(int value){
  speed = value;

  // Let the Client know that we are done
  // We must always respond to the Client
  return "done";
}

