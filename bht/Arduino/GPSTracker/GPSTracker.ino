/*
*  (C) Dave Robertson 
*
*
*/

#if ARDUINO >= 100
 #include "Arduino.h"
 #include "SoftwareSerial.h"
#else
 #include "WProgram.h"
 #include "NewSoftSerƒial.h"
#endif

#include <GSM_Shield.h>

// Use pins 2 and 3 to talk to the GPS. 2 is the TX pin, 3 is the RX pin
#if ARDUINO >= 100
SoftwareSerial GPSSerial = SoftwareSerial(10,11);
#else
NewSoftSerial GPSSerial = NewSoftSerial(10, 11);
#endif

// Use pin 4 to control power to the GPS
#define powerpin 12

// Set the GPSRATE to the baud rate of the GPS module. Most are 4800
// but some are 38400 or other. Check the datasheet!
#define GPSRATE 4800
//#define GPSRATE 38400

// The buffer size that will hold a GPS sentence. They tend to be 80 characters long
// so 90 is plenty.
#define BUFFSIZ 90 // plenty big
#define SEND 30000 // Number of millies between a GSM Send


// global variables
char buffer[BUFFSIZ];        // string buffer for the sentence
char *parseptr;              // a character pointer for parsing
char buffidx;                // an indexer into the buffer

GSM gsm;


// The time, date, location data, etc.
uint8_t hour, minute, second, year, month, date;
uint32_t latitude, longitude;
uint8_t groundspeed, trackangle;
char latdir, longdir;
char status;
long gmLatitude;
long gmLongitude;

unsigned long GPSTime;
unsigned long GPSLastTime;

void setup() 
{ 
  
  gsm.TurnOn(9600);          //module power on
  Serial.println("Waiting for GSM"); 
  
  //delay(5000);
  
  gsm.InitParam(PARAM_SET_1);//configure the module  
  gsm.Echo(1);               //enable AT echo 

  Serial.println("GSM Initalised"); 

  
  if (powerpin) {
    pinMode(powerpin, OUTPUT);
  }
  
  // Use the pin 13 LED as an indicator
  pinMode(13, OUTPUT);
  
  // connect to the serial terminal at 9600 baud
  Serial.begin(9600);
  
  // connect to the GPS at the desired rate
  GPSSerial.begin(GPSRATE);
   
  // prints title with ending line break 
  Serial.println("GPS parser - Waiting to Aquire Sats"); 
 
   digitalWrite(powerpin, HIGH);         // pull low to turn on!
   digitalWrite(13, LOW);         // pull low to turn on!
} 
 
uint32_t parsedecimal(char *str) {
  uint32_t d = 0;
  
  while (str[0] != 0) {
   if ((str[0] > '9') || (str[0] < '0'))
     return d;
   d *= 10;
   d += str[0] - '0';
   str++;
  }
  return d;
}

void readline(void) {
  char c;
  
  buffidx = 0; // start at begninning
  while (1) {
      c=GPSSerial.read();
      if (c == -1)
        continue;
      //Serial.print(c);
      if (c == '\n')
        continue;
      if ((buffidx == BUFFSIZ-1) || (c == '\r')) {
        buffer[buffidx] = 0;
        return;
      }
      buffer[buffidx++]= c;
  }
}

void loop()


{
    GPSCheck();
}


void GSMSend(char *GPSLine)
 {
 
char GPRMC[128];

    strcpy(GPRMC,"dhrobertson.com/bht/update.php?gps=");
    strcat(GPRMC,GPSLine);
    
    Serial.println("Sending Update via GSM");
    Serial.println(GPRMC);
      
    gsm.sendHTTP(GPRMC,"OK 1");
    GPSSerial.listen();
     
         
   
 }
 
 
void GPSCheck() 
{ 
  uint32_t tmp;
  float latf,longf;
  char latdec[20];
  char longdec[20];
  char lat_deg[4];
  char min1[3];
  char min2[3];
    //String lat_min3 = "0." + lat_min1 + lat_min2;
    //float lat_dec = Float.parseFloat(lat_min3)/.6f;
    //float lat_val = Float.parseFloat(lat_deg) + lat_dec;
 
  
   GPSSerial.listen();
   readline();    // Read a single line
  
    // check if $GPRMC (global positioning fixed data)
   if (strncmp(buffer, "$GPRMC",6) == 0) {

     // Timer here - do we want to do anythig with it 
     GPSTime = millis();
      
     if ( (GPSTime - GPSLastTime) < SEND )  {
       return;  // Dont wait - just exit the loop we already read the buffer
     }

     GPSLastTime = GPSTime;    

     // Send the whole buffer out
     // hhmmss time data
     parseptr = buffer+7;
     tmp = parsedecimal(parseptr); 
     hour = tmp / 10000;
     minute = (tmp / 100) % 100;
     second = tmp % 100;
    
     parseptr = strchr(parseptr, ',') + 1;
     status = parseptr[0];
     parseptr += 2;
 
     if (status == 'A' ) {

       Serial.print("http://dhrobertson.com/bht/update.php?gps=");    
       Serial.print(buffer);
       Serial.print("\n\r");
      
       GSMSend(buffer);
      
       digitalWrite(13, HIGH);         // pull low to turn on!
    
      }
      else {
        digitalWrite(13, LOW);         // pull low to turn on!
     }
 
   }    
}

