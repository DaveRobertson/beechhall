<?php

// Updates the database with a valid lat and long position from the device.


// Display the database entry 
if(isset($_GET['display']) ) {

  /* soak in the passed variable or set our own */	
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
  //$user_id = intval($_GET['user']); //no default
  // Change the line below to your timezone!
	date_default_timezone_set('Europe/London');
	$date = date('m/d/Y h:i:s a');
  
	/* connect to the db */
	$link = mysql_connect('localhost','dhrober1_bht','HotDog57') or die('Cannot connect to the DB');
	mysql_select_db('dhrober1_bhtracker',$link) or die('Cannot select the DB');

	/* grab the positions from the db */
	$query = "SELECT * from buslocation ";
	$result = mysql_query($query,$link) or die('Update failed:  '.$query);

  /* create one master array of the records */
  $posts = array();
  if(mysql_num_rows($result)) {
    while($post = mysql_fetch_assoc($result)) {
      $posts[] = array('post'=>$post);
    }
  }

  /* output in necessary format */
  if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode(array('posts'=>$posts));
  }
  else {
    header('Content-type: text/xml');
    echo '<posts>';
    foreach($posts as $index => $post) {
      if(is_array($post)) {
        foreach($post as $key => $value) {
          echo '<',$key,'>';
          if(is_array($value)) {
            foreach($value as $tag => $val) {
              echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
            }
          }
          echo '</',$key,'>';
        }
      }
    }
    echo '</posts>';
  }
  
  /* disconnect from the db */
	@mysql_close($link);
}


/* require the lat and long as the parameter */
if(isset($_GET['lat']) && isset($_GET['lon']) ) {

//if(isset($_GET['lat']) && is_double($_GET['lat'])) {

  /* soak in the passed variable or set our own */
	$bus = isset($_GET['bus']) ? intval($_GET['bus']) : 1; // Bus 1 is the default
	$lat = $_GET['lat'];
	$lon = $_GET['lon'];
	
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
  //$user_id = intval($_GET['user']); //no default
  // Change the line below to your timezone!
	date_default_timezone_set('Europe/London');
	$date = date('m/d/Y h:i:s a');
  
	echo $date;
	echo "<BR>";
  
	//setlocale( "LC_ALL", "" ); 
    $date = $_SERVER['REQUEST_TIME'] ;
    
    //$now = time(); 
    echo "<BR>";
	//echo $now;
	echo "<BR>";
	echo $date;
	
  /* connect to the db */
  $link = mysql_connect('localhost','dhrobert1_bht','HotDog57') or die('Cannot connect to the DB');
  mysql_select_db('dhrober1_bhtracker',$link) or die('Cannot select the DB');

  
  // UPdate the database
  
	$sql = "insert into buslocation (bus, lat, lng, updated_at, created_at)"; 
	$sql .= "values(1,$lat,$lon, NOW(), NOW())";
	$result = mysql_query($sql,$link) or die('Update of lat/longfailed:  '.$query);
  
	/* disconnect from the db */
	@mysql_close($link);
}

/* require the lat and long as the parameter */
if( isset($_GET['gps'])  ) {

	//Set timezone
	date_default_timezone_set('Europe/London');

	//Connect to GPS
	//$gps = fopen("/dev/tty.iBT-GPS-SPPslave", "r+");

	$gps = $_GET['gps'];


	// Argument must be like this to the update.php location
	//$gps='$GPRMC,110750.000,A,5315.4706,N,00206.5109,W,2.28,83.18,110812,,,A*41';

	$theTime = date('Y-m-d H:i:s');
	
	echo "Time now is: " .$theTime . "<BR>";
	
	
	$buffer=$gps;

	//Read data from GPS
	if(substr($buffer, 0, 6)=='$GPRMC'){
		
		//echo $buffer."<BR>";
	
		$gprmc = explode(',',$buffer);
		
		//echo "<BR>";
		//print_r ($gprmc) ;
		//echo "<BR>";
		
		$data['timestamp'] = strtotime('now');
		$data['sat_status'] = $gprmc[2];

		$data['lattitude_dms'] = $gprmc[3];
		$data['lattitude_decimal'] = DMStoDEC($gprmc[3],'lattitude');
		$data['lattitude_direction'] = $gprmc[4];
		$latdir=$gprmc[4];
		
		$data['longitude_dms'] = $gprmc[5];
		$data['longitude_decimal'] = DMStoDEC($gprmc[5],'longitude');
		$data['longitude_direction'] = $gprmc[6];
		$longdir=$gprmc[6];
		
		$data['speed_knots'] = $gprmc[7];

		$data['bearing'] = $gprmc[8];
		
		$data['utc'] = $gprmc[9];
		
		$data['google_map'] = '<BR>http://maps.google.com/maps?q='.$data['lattitude_decimal'].','.$data['longitude_decimal'].'+(PHP Decoded)&iwloc=A';
	
		//print_r($data);
		//echo "<BR>";
		
		//setlocale( "LC_ALL", "" ); 
		$date = $_SERVER['REQUEST_TIME'] ;
    
		//$now = time(); 
		//echo "<BR>";
		//echo $now;
		//echo "<BR>";
		//echo $date;
		//echo "Connecting to database <BR>";
		/* connect to the db */

		//echo "<BR> Lattritude: " + $latdir + "<BR>";
		// UPdate the database
		if ($latdir == 'N')
			$lat= $data['lattitude_decimal'];
		else if ($latdir == 'S')
			$lat= "-" . $data['lattitude_decimal'];
		
		//echo $longdir + "<BR>";
		if ($longdir == 'E')
			$lng=$data['longitude_decimal'];
		else if ($longdir == 'W')
			$lng="-" . $data['longitude_decimal'];
    
		
		$speed=$data['speed_knots'];
		$bearing=$data['bearing'];
		$utc=$data['utc'];

		//echo "<BR> Connecting to database using localhost and dhrober1_bht" ;
		
		
		$link = mysql_connect('localhost','dhrober1_bht','HotDog57') or die('ERROR: Cannot connect to the DB');
		mysql_select_db('dhrober1_bhtracker',$link) or die('ERROR: Cannot select the DB');
		
  
		$sql = "insert into buslocation (bus, lat, lng, speed,bearing,utc,updated_at, created_at)"; 
		$sql .= "VALUES(1,$lat,$lng,$speed,$bearing ,$utc,'".$theTime."','".$theTime."')"; 
		
		//$sql .= "values(1,$lat,$lng,$speed,$bearing ,$utc, $theTime, $theTime)";
		//$sql .= "VALUES(1,$lat,$lng,$speed,$bearing ,$utc,$theTime,$theTime)";
		
		// $query = "INSERT INTO item(name, created_on), VALUES('date example',".$current_date.")";  
		
		$result = mysql_query($sql,$link) or die('ERROR: Update of lat/longfailed:  '.$sql . '<BR>');
		
  
		/* disconnect from the db */
		@mysql_close($link);
		
		
		echo "OK " . $result;
		
	}
}
		



// Converts DMS ( Degrees / minutes / seconds ) 
// to decimal format longitude / latitude
function DMStoDEC($dms, $longlat){

	if($longlat == 'lattitude'){
		$deg = substr($dms, 0, 2);
		$min = substr($dms, 2, 8);
		$sec = '';
	}
	if($longlat == 'longitude'){
		$deg = substr($dms, 0, 3);
		$min = substr($dms, 3, 8);
		$sec='';
	}
	

    return $deg+((($min*60)+($sec))/3600);
}   



?>