use dhrober1_bhtracker;
CREATE TABLE dhrober1_bhtracker.buslocation (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
bus VARCHAR( 60 ) NOT NULL ,
lat FLOAT( 10, 6 ) NOT NULL ,
lng FLOAT( 10, 6 ) NOT NULL ,
speed FLOAT( 10, 6 ) NOT NULL ,
bearing FLOAT( 10, 6 ) NOT NULL ,
utc VARCHAR(10) NOT NULL,
updated_at          TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
created_at          DATETIME DEFAULT NULL
  
) ENGINE = MYISAM ;